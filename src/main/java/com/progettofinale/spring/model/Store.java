package com.progettofinale.spring.model;

import java.util.ArrayList;

public class Store {
	private Integer idStore;
	private String nomeS;
	private String indirizzo;
	private ArrayList<Prodotto> elencoProdotti = new ArrayList<Prodotto>();
	
	public Store() {
		
	}

	public Integer getIdStore() {
		return idStore;
	}

	public void setIdStore(Integer idStore) {
		this.idStore = idStore;
	}

	public String getNomeS() {
		return nomeS;
	}

	public void setNomeS(String nomeS) {
		this.nomeS = nomeS;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public ArrayList<Prodotto> getElencoProdotti() {
		return elencoProdotti;
	}
	
	public void addProdotto(Prodotto prodotto) {
		this.elencoProdotti.add(prodotto);
	}

	public void setElencoProdotti(ArrayList<Prodotto> elencoProdotti) {
		this.elencoProdotti = elencoProdotti;
	}
}
