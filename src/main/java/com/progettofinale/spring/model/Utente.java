package com.progettofinale.spring.model;

public class Utente {
	private Integer idUtente;
	private String usernameU;
	private String pswU;
	private String tipologiaU;
	private String indirizzo;
	
	public Utente() {
		
	}

	public Integer getIdUtente() {
		return idUtente;
	}

	public void setIdUtente(Integer idUtente) {
		this.idUtente = idUtente;
	}

	public String getUsernameU() {
		return usernameU;
	}

	public void setUsernameU(String usernameU) {
		this.usernameU = usernameU;
	}

	public String getPswU() {
		return pswU;
	}

	public void setPswU(String pswU) {
		this.pswU = pswU;
	}

	public String getTipologiaU() {
		return tipologiaU;
	}

	public void setTipologiaU(String tipologiaU) {
		this.tipologiaU = tipologiaU;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
}
